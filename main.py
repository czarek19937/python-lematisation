# encoding=UTF-8
from TF.tf import Tf
from TFIDF.tfidf import Tfidf
from RAKE.rake import Rake
from LDA.lda import Lda
from LSA.lsa import Lsa
import operator

stopwords = "polish.stopwords.txt"
lematized_dir = "LEMATIZED_DOCUMENTS/"

print("\n\nTF!:")
tf_object = Tf(stopwords, lematized_dir)
tf_matrix = tf_object.run()
print(tf_matrix)

print("\n\nTF-IDF!:")
tfidf_object = Tfidf(stopwords, lematized_dir)
tfidf_data = tfidf_object.run()
print("All:\n" + str(tfidf_data))
print("Only 3:\n" + str(tfidf_data[:3]))

print("\n\nLDA!:")
lda_object = Lda(stopwords, lematized_dir, 5, 3)

lda_keywords = lda_object.run()
print (lda_keywords)


print("\n\nLSA!:")
lsa_object = Lsa(stopwords, lematized_dir, 1)
lsa_keywords = lsa_object.run()
print (lsa_keywords)


print("\n\nRAKE!")
rake_object = Rake(stopwords, 3, 3, 1)
text = "Za artykuł naukowy należy rozumieć artykuł prezentujący wyniki oryginalnych badań o charakterze empirycznym, teoretycznym, technicznym lub analitycznym zawierający tytuł publikacji, nazwiska i imiona autorów wraz z ich afiliacją i przedstawiający obecny stan wiedzy, metodykę badań, przebieg procesu badawczego, jego wyniki oraz wnioski, z przytoczeniem cytowanej literatury (bibliografię). Do artykułów naukowych zalicza się także opublikowane w czasopismach naukowych opracowania o charakterze monograficznym, polemicznym lub przeglądowym jak również glosy lub komentarze prawnicze. "
keywords = rake_object.run(text)
print (keywords)
