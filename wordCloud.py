#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Minimal Example
===============
Generating a square wordcloud from the US constitution using default arguments.
"""

from os import path
from wordcloud import WordCloud
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

debug = False
tests = False

class WordCloudChart():
    """
        data: dict
        {
            word1: frequency1,
            word2: frequency2,
            ...
        }
    """
    def run(self, data, fileName):
        for key, value in data.items():
            data[self.removePolishSigns(key)] = data.pop(key)
            data[key] = self.removeZeroValues(value)

        wordcloud = WordCloud(background_color='black', max_font_size=40, height=200, width=200).generate_from_frequencies(data)

        plt.figure()
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.axis("off")
        plt.savefig('./images/' + fileName + '.png')
        plt.close()

    def removePolishSigns(self, data):
        return data.replace("ę", "e").replace("ś", "s").replace("ć", "c").replace("ó", "o").replace("ą", "a").replace("ż", "z").replace("ł", "l")

    def removeZeroValues(self, data):
        if data > 0:
           return data
        if data == 0:
           return float(0.0001);
        else:
           return float(0.0001);


if tests:
    d = path.dirname(__file__)
    data = {'artykuł': 10, 'borówka': 9, 'lubić': 8, 'charakter': 5, 'naukowy': 3, 'wynik': 2, 'badanie': 1}
    wordCloudChart = WordCloudChart()
    wordCloudChart.run(data, 'example_wordcloud')
