#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

debug = False
tests = False

class BarChart():
    """
        charTitle: string,
        xAxisName: string,
        yAxisName: string,
        data: dict
        {
            word1: frequency1,
            word2: frequency2,
            ...
        }
    """
    def run(self, chartTitle, xAxisName, yAxisName, data, fileName):
        # name of each bar and it value
        wordsList = []
        wordsValue = []
        for key, value in data.items():
            no_pl = self.removePolishSigns(key)
            data[no_pl] = data.pop(key)
            data[no_pl] = self.makeAbsoluteValues(value)

        for key, value in data.items():
            wordsList.append(key)
            wordsValue.append(value)
        n_groups = len(wordsList)
        fig, ax = plt.subplots()
        index = np.arange(n_groups)
        bar_width = 0.5
        plt.barh(index, wordsValue, bar_width, label='Slowa')

        plt.xlabel(xAxisName)
        plt.ylabel(yAxisName)
        plt.title(chartTitle)

        plt.yticks(index, wordsList)
        plt.legend()

        plt.tight_layout()
        fig.savefig('./images/' + fileName + '.png')
        plt.close(fig)

    def removePolishSigns(self, data):
        return data.replace("ę", "e").replace("ś", "s").replace("ć", "c").replace("ó", "o").replace("ą", "a").replace("ż", "z").replace("ł", "l")

    def makeAbsoluteValues(self, data):
        if data < 0:
           return math.fabs(data)
        else:
            return data

if tests:
    data = {'artykuł': 10, 'borówka': 9, 'lubić': 8, 'charakter': 5, 'naukowy': 3, 'wynik': 2, 'badanie': 1}
    chart = BarChart()
    chart.run("example chart title", "example x axis name", "example y axis name", data, 'example_chart')
