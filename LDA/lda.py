#
# https://rstudio-pubs-static.s3.amazonaws.com/79360_850b2a69980c4488b1db95987a24867a.html
#
# Zwraca:
# Znacznie słów w dokumencie w formie prawdopodobnieństwo * słowo
#
# Konstruktor przyjmuje:
# plik ze slowami do usuniecie, ilosc tematow, ilosc slow w temacie
#


from gensim import corpora, models
import gensim
import re
import os

debug = False
test = False

def load_stop_words(stop_word_file):
    """
    Utility function to load stop words from a file and return as a list of words
    @param stop_word_file Path and file name of a file containing stop words.
    @return list A list of stop words.
    """
    if debug: print("opening file:" + stop_word_file)
    stop_words = []
    for line in open(stop_word_file):
        if line.strip()[0:1] != "#":
            for word in line.split():  # in case more than one per line
                stop_words.append(word)
    if debug: print("Loaded : " + str(len(stop_words)) + " stopwords")
    return stop_words

def load_lematized_documents(directory):
    if debug: print("Getting docs")
    docs = []
    for fn in os.listdir(directory):
        document = ''
        document = open(directory + fn, 'r')
        docs.append(document.read())
        document.close()
    return docs

def remove_stop_words(doc_set, stopwords):
    # list for tokenized documents in loop
    texts = []

    # loop through document list
    for i in doc_set:
    # clean and tokenize document string
        raw = i.lower()
        if debug: print("Maked raw: " + raw)
        tokens = raw.split()

        # remove stop words from tokens
        stopped_tokens = [i for i in tokens if not i in stopwords]
        # add tokens to list
        texts.append(stopped_tokens)
    return texts

def make_dictionary(texts):
    # turn our tokenized documents into a id <-> term dictionary
    if debug: print("Loaded: " + str(len(texts)) + " documents")
    dictionary = corpora.Dictionary(texts)
    if debug:
        for key in dictionary.values():
            print("dictionary value: " + key)
    return dictionary

def create_lda_model(dictionary, texts, topics):
    # convert tokenized documents into a document-term matrix
    corpus = [dictionary.doc2bow(text) for text in texts]

    # generate LDA model
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=topics, id2word = dictionary)
    return ldamodel

def get_ldamodel_topics(ldamodel, topics, words):
   return ldamodel.print_topics(num_topics=topics, num_words=words)

class Lda(object):
    # lda generuje tematyka dokumentow
    # z jakim prawdopodobeistwem to slowo jest zgodne z tematyka
    # lda wagi wyrazow, jesli slowo wystepuje na poczatku albo na koncu to jest wazniejsze
    # topics ile temato wyciagasz, moze ile chcesz
    # words ile slow w danym temacie
    def __init__(self, stop_words_path, directory, topics=3, words=3):
        self.__stop_words_path = stop_words_path
        self.__topics = topics
        self.__words = words
        self.__directory = directory

    def run(self):
        stopwords = load_stop_words(self.__stop_words_path)
        if debug: print("run: stopwords")
        doc_set = load_lematized_documents(self.__directory)
        if debug: print("run: docset")
        texts = remove_stop_words(doc_set, stopwords)
        if debug: print("run: texts")
        dictionary = make_dictionary(texts)
        if debug: print("run: dictionary")
        ldamodel = create_lda_model(dictionary, texts, self.__topics)
        if debug: print("run: ldamodel")
        return get_ldamodel_topics(ldamodel, self.__topics, self.__words)

if test:
    stoppath = "../polish.stopwords.txt"
    directory = "../LEMATIZED_DOCUMENTS/"
    stopwords = load_stop_words(stoppath)

    doc_set = load_lematized_documents(directory)

    texts = remove_stop_words(doc_set, stopwords)

    dictionary = make_dictionary(texts)

    ldamodel = create_lda_model(dictionary, texts, 2)

    topics = get_ldamodel_topics(ldamodel, 2, 3)

    print(topics)

    lda_object = Lda(stoppath, directory, 5, 5)
    keywords = lda_object.run()
    print (keywords)
