# encoding=UTF-8

# Copyright © 2007-2017 Jakub Wilk <jwilk@jwilk.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys

if sys.version_info >= (2, 7):
    import unittest
else:
    import unittest2 as unittest

import morfeusz

if str is bytes:
    def u(s):
        return s.decode('UTF-8')
else:
    def u(s):
        return s

sgjp = 'SGJP' in morfeusz.about()

class test_analyse(unittest.TestCase):

    def test1(self):
        text = u('Bardzo lubię jeść borówki. Borówki są bardzo dobre. '
                 'Lubię chodzić do lasu, który jest daleko ode mnie.'
                 'Tam są jagody które bardzo lubie i jeżyny, których nie lubię')
        basicForm = ""
        basicFormList = []
        for s in morfeusz.analyse(text, dag=True):
            if s[2][1] not in basicFormList and s[2][1] is not None:
                basicFormList.append(s[2][1])
                basicForm += s[2][1] + " "
        self.assertEqual(u("bardzo lubić jeść borówka . być dobry chodzić do las , który daleko od ja miąć tam tama jagoda i jeżyna nie on "), basicForm)

if __name__ == '__main__':
    unittest.main()
