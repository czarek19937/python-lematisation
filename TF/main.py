# encoding=UTF-8

# Copyright © 2007-2017 Jakub Wilk <jwilk@jwilk.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import RAKE
from gensim import corpora
import logging
from collections import defaultdict
from pprint import pprint  # pretty-printer
# logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


if sys.version_info >= (2, 7):
    import unittest
else:
    import unittest2 as unittest

import morfeusz

if str is bytes:
    def u(s):
        return s.decode('UTF-8')
else:
    def u(s):
        return s

sgjp = 'SGJP' in morfeusz.about()

class test_analyse(unittest.TestCase):

    def test1(self):
        text = u('Bardzo lubię jeść borówki. Borówki są bardzo dobre. '
                 'Lubię chodzić do lasu, który jest daleko ode mnie.'
                 'Tam są jagody które bardzo lubie i jeżyny, których nie lubię')
        basicForm = ""
        basicFormList = []
        for s in morfeusz.analyse(text, dag=True):
            if s[2][1] not in basicFormList and s[2][1] is not None:
                basicFormList.append(s[2][1])
                basicForm += s[2][1] + " "
        self.assertEqual(u("bardzo lubić jeść borówka . być dobry chodzić do las , który daleko od ja miąć tam tama jagoda i jeżyna nie on "), basicForm)

    def test2(self):
        text = u('Bardzo lubię jeść borówki. Borówki są bardzo dobre. '
                 'Lubię chodzić do lasu, który jest daleko ode mnie.'
                 'Tam są jagody które bardzo lubie i jeżyny, których nie lubię.')
        # bardzo lubić jeść borówka . być dobry chodzić do las , który daleko od ja miąć tam tama jagoda
        basicForm = ""
        basicFormList = []
        # text = text.replace(",", "").replace(".", "").replace(" i ", "").replace(" lub ", "")
        text = text.replace(",", "").replace(".", "").replace(" i ", "").replace(" lub ", "")
        for s in morfeusz.analyse(text, dag=True):
            if s[2][1] not in basicFormList and s[2][1] is not None:
                basicFormList.append(s[2][1])
                basicForm += s[2][1] + " "

        print(basicForm)
        print(basicFormList)

        # documents = ["Human machine interface for lab abc computer applications",
        #               "A survey of user opinion of computer system response time",
        #               "The EPS user interface management system",
        #               "System and human system engineering testing of EPS",
        #               "Relation of user perceived response time to error measurement",
        #               "The generation of random binary unordered trees",
        #               "The intersection graph of paths in trees",
        #               "Graph minors IV Widths of trees and well quasi ordering",
        #               "Graph minors A survey"]
        #
        # # remove common words and tokenize
        # stoplist = set('for a of the and to in'.split())
        # texts = [[word for word in document.lower().split() if word not in stoplist]
        #          for document in documents]
        #
        # # remove words that appear only once
        # frequency = defaultdict(int)
        # for text in texts:
        #     for token in text:
        #         frequency[token] += 1
        #
        # texts = [[token for token in text if frequency[token] > 1]
        #          for text in texts]
        # stoplist = set('lub a od . ,'.split())
        texts = [basicFormList]
        print(texts)
        dictionary = corpora.Dictionary(texts)
        dictionary.save('/tmp/deerwester.dict')
        print(dictionary)

        print(dictionary.token2id)
        # new_doc = "Human computer interaction with graph minors and graph minors"
        new_doc = "Ja lubię jeść borówki i lubię też jeść jeżyny"
        docForm = ""
        docFormList = []

        print(new_doc)
        for s in morfeusz.analyse(new_doc, dag=True):
            if s[0] not in docFormList and s[2][1] is not None:
                docFormList.append(s[0])
                docForm += s[2][1] + " "

        print("doc form")
        print(docFormList)
        print(docForm)
        new_vec = dictionary.doc2bow(docForm.lower().split())
        # print("Human computer interaction with graph minors and graph minors")
        # print("lubić jeść borówka")
        print(new_vec)  # the word "interaction" does not appear in the dictionary and is ignored
        occurencs_sum = 0
        for vec in new_vec:
            print(vec[1])
            occurencs_sum += vec[1]
        self.assertEqual(occurencs_sum, 5)
        corpus = [dictionary.doc2bow(text) for text in texts]

        corpora.MmCorpus.serialize('/tmp/deerwester.mm', corpus)  # store to disk, for later use
        print(corpus)
        print("tutaj mamy tf i jest git trzeba zrefactorowac")



# self.assertEqual(u("bardzo lubić jeść borówka "), basicForm)

if __name__ == '__main__':
    unittest.main()
