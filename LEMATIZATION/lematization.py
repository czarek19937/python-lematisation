# encoding=UTF-8

import sys, os

if sys.version_info >= (2, 7):
    import unittest
else:
    import unittest2 as unittest

import morfeusz

if str is bytes:
    def u(s):
        return s.decode('UTF-8')
else:
    def u(s):
        return s

sgjp = 'SGJP' in morfeusz.about()

tests = False

class Lematization(object):
     def __init__(self, documents_dir, lematized_dir):
        self.__documents_dir = documents_dir
        self.__lematized_dir = lematized_dir

     def run(self):
        counter = 0;
        for fn in os.listdir(self.__documents_dir):
            document = ''
            document = open(self.__documents_dir + fn, 'r')
            basicForm = ""
            basicFormList = []
            for s in morfeusz.analyse(document.read().replace('\n', ' ').replace('\r', '').replace('.', '').replace(',', ''), dag=True):
                if s[0] not in basicFormList and s[2][1] is not None:
                    basicFormList.append(s[0])
                    basicForm += s[2][1] + " "
            document.close()
            counter += 1
            morfeuszResults = open(self.__lematized_dir + fn, 'w+')
            morfeuszResults.write(basicForm)
            morfeuszResults.close()

        print("Lematyzacja " + str(counter) + " dokumentow.")

if tests:
    lema = Lematization("../DOCUMENTS", "../LEMATIZED_DOCUMENTS/")
    lema.run()
