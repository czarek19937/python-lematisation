# encoding=UTF-8

import sys, os

if sys.version_info >= (2, 7):
    import unittest
else:
    import unittest2 as unittest

import morfeusz

if str is bytes:
    def u(s):
        return s.decode('UTF-8')
else:
    def u(s):
        return s

sgjp = 'SGJP' in morfeusz.about()

class test_analyse(unittest.TestCase):

    def test_basic_morfeusz(self):
        text = u('Bardzo lubię jeść borówki. Borówki są bardzo dobre. '
                 'Lubię chodzić do lasu, który jest daleko ode mnie.'
                 'Tam są jagody które bardzo lubie i jeżyny, których nie lubię')
        basicForm = ""
        basicFormList = []
        for s in morfeusz.analyse(text, dag=True):
            if s[2][1] not in basicFormList and s[2][1] is not None:
                basicFormList.append(s[2][1])
                basicForm += s[2][1] + " "
        self.assertEqual(u("bardzo lubić jeść borówka . być dobry chodzić do las , który daleko od ja miąć tam tama jagoda i jeżyna nie on "), basicForm)

    def test_advanced_morfeusz(self):
        for fn in os.listdir('../DOCUMENTS'):
            document = ''
            document = open("../DOCUMENTS/" + fn, 'r')
            basicForm = ""
            basicFormList = []
            for s in morfeusz.analyse(document.read().replace('\n', ' ').replace('\r', '').replace('.', '').replace(',', ''), dag=True):
                if s[0] not in basicFormList and s[2][1] is not None:
                    basicFormList.append(s[0])
                    basicForm += s[2][1] + " "
            document.close()
            morfeuszResults = open('../LEMATIZED_DOCUMENTS/' + fn, 'w+')
            morfeuszResults.write(basicForm)
            morfeuszResults.close()
            self.assertTrue(os.path.exists('../LEMATIZED_DOCUMENTS/' + fn))

    # unused
    def checkIsExpressionInStopwords(self, expression):
        stopwords_file = open('../polish.stopwords.txt')
        for line in stopwords_file:
            if line.strip()[0:1] != "#":
                for word in line.split():  # in case more than one per line
                    if expression == word:
                        stopwords_file.close()
                        return True
        stopwords_file.close()
        return False


if __name__ == '__main__':
    unittest.main()
