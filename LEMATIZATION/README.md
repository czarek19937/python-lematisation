# TO RUN Example test 
LD_LIBRARY_PATH=$PWD python main.py
sudo LD_LIBRARY_PATH=$PWD python3.5 main.py
in any cases
dist: trusty
sudo: false
language: python
python:
- "2.6"
- "2.7"
- "3.2"
- "3.3"
- "3.4"
- "3.5"
- "3.6"
- "3.7-dev"
script:
- if [ "$TRAVIS_PYTHON_VERSION" = '2.6' ]; then pip install unittest2; fi
- wget http://sgjp.pl/morfeusz/morfeusz-siat/morfeusz-linux64-20080205.tar.bz2
- tar -xvf morfeusz-linux64-20080205.tar.bz2
- LD_LIBRARY_PATH=$PWD python tests.py
- LC_ALL=C python setup.py --version

# vim:ts=2 sts=2 sw=2 et