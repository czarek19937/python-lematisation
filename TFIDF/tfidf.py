# https://radimrehurek.com/gensim/models/tfidfmodel.html
# Book:
# Text Analytics with Python: A Practical Real-World Approach to Gaining Actionable
#                             Insights from your Data (page 233)
# Written by: Dipanjan Sarkar
from gensim import corpora
from gensim.models import TfidfModel
import operator
import os
from collections import defaultdict
from pprint import pprint

debug = False
tests = False

def load_stop_words(stop_word_file):
    """
    Utility function to load stop words from a file and return as a list of words
    @param stop_word_file Path and file name of a file containing stop words.
    @return list A list of stop words.
    """
    if debug: print("opening file:" + stop_word_file)
    stop_words = []
    for line in open(stop_word_file):
        if line.strip()[0:1] != "#":
            for word in line.split():  # in case more than one per line
                stop_words.append(word)
    if debug: print("Loaded : " + str(len(stop_words)) + " stopwords")
    return stop_words

def load_lematized_documents(directory):
    if debug: print("Getting docs")
    docs = []
    for fn in os.listdir(directory):
        document = ''
        document = open(directory + fn, 'r')
        docs.append(document.read())
        document.close()
    return docs

def remove_stop_words(doc_set, stopwords):
    # list for tokenized documents in loop
    texts = []

    # loop through document list
    for i in doc_set:
    # clean and tokenize document string
        raw = i.lower()
        if debug: print("Maked raw: " + raw)
        tokens = raw.split()

        # remove stop words from tokens
        stopped_tokens = [i for i in tokens if not i in stopwords]
        # add tokens to list
        texts.append(stopped_tokens)
    return texts

def remove_words_that_appear_once(texts):
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1

    return_texts = [[token for token in text if frequency[token] > 1]
            for text in texts]
    return return_texts

def make_dictionary(cleaned_texts):
    return corpora.Dictionary(cleaned_texts)

class Tfidf(object):
    def __init__(self, stop_words_path, directory):
        self.__stop_words_path = stop_words_path
        self.__directory = directory

    def run(self):
        stopwords = load_stop_words(self.__stop_words_path)
        if debug: print("ran: stopwords")

        doc_set = load_lematized_documents(self.__directory)
        if debug: print("ran: docset")

        texts = remove_stop_words(doc_set, stopwords)
        if debug: print("ran: texts")

        cleaned_texts = remove_words_that_appear_once(texts)
        if debug: print("ran: Remove words that once")

        dictionary = make_dictionary(cleaned_texts)
        if debug: print("ran: dictionary")

        corpus = [dictionary.doc2bow(text) for text in cleaned_texts]
        if debug: print("ran: corpus builded")

        # robi magie czyli oblicza czestosci
        tfidf_model = TfidfModel(corpus)
        if debug: print("ran: TFIDF model builded")

        #
        corpus_tfidf = tfidf_model[corpus]
        if debug: print("ran: TF simple indexes to words")

        weighted_phrases = {dictionary.get(id): round(value,3)
                            for doc in corpus_tfidf
                            for id, value in doc}
        if debug: print("ran: Changed indexes to words")

        return_phrases = sorted(weighted_phrases.items(), key=operator.itemgetter(1), reverse=True)
        if debug: print("ran: Sorted phrases by values DESCENDING")

        # wazona czestosc wystepowania terminow
        return return_phrases

if tests:
    stoppath = "../polish.stopwords.txt"
    directory = "../LEMATIZED_DOCUMENTS/"

    tfidf_object = Tfidf(stoppath, directory)
    keywords = tfidf_object.run()
    print (keywords[:3])
