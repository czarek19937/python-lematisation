from operator import itemgetter
import os
from TF.tf import Tf
from TFIDF.tfidf import Tfidf
from RAKE.rake import Rake
from LDA.lda import Lda
from LSA.lsa import Lsa
from LEMATIZATION.lematization import Lematization
from barChart import BarChart
from wordCloud import WordCloudChart
import subprocess
import operator

stopwords = "polish.stopwords.txt"
lematized_dir = "LEMATIZED_DOCUMENTS/"
documents_dir = "DOCUMENTS/"

menu = {}
menu['1']="Lematyzacja dokumentow z katalogu DOCUMENTS"
menu['2']="TF"
menu['3']="TF-IDF"
menu['4']="LDA"
menu['5']="LSA"
menu['6']="RAKE"
menu['0']="EXIT"

def clean():
    os.system('cls' if os.name == 'nt' else 'clear')

clean()
while True:
    print("************** MENU ********************")
    options=sorted(menu.keys())
    for entry in options: 
      print (entry, menu[entry])
    print("****************************************")

    selection=input("Wybierz Opcje: ") 
    if selection =='1':
      clean() 
      print("************ LEMATYZACJA ***************")
      lem = Lematization(documents_dir, lematized_dir)
      lem.run()
      print("\n\n")
    elif selection == '2': 
      clean()
      print("***************** TF *******************")
      tf_object = Tf(stopwords, lematized_dir)
      tf_matrix = tf_object.run()
      chart = BarChart()
      chart.run("Wykres TF", "Czestosc", "Slowa", tf_matrix, 'tf_chart')
      wordcloud = WordCloudChart()
      wordcloud.run(tf_matrix, 'tf_word_cloud')
      print(tf_matrix)
      print("chart generated")
      print("wordcloud generated")
      print("\n\n")
    elif selection == '3':
      clean()
      print ("*************** TF-IDF *****************")
      how_many = int(input("Ile elementow chcesz wyswietlic: "))
      tfidf_object = Tfidf(stopwords, lematized_dir)
      tfidf_data = tfidf_object.run()
      data_dict = {}
      for key, value in enumerate(tfidf_data[:how_many]):
          data_dict[value[0]] = value[1]
      chart = BarChart()
      chart.run("Wykres TF-IDF", " ", "Slowa", data_dict, 'tf_idf_chart')
      wordcloud = WordCloudChart()
      wordcloud.run(data_dict, 'tf_idf_word_cloud')
      print("chart generated")
      print("wordcloud generated\n")
      print(data_dict)
      print("\n\n")
    elif selection == '4':
      clean()
      print ("****************** LDA *****************")
      topics = int(input("Ile tematow wygenerowac: "))
      words = int(input("Ile slow ma miec temat: "))
      lda_object = Lda(stopwords, lematized_dir, topics, words)
      lda_keywords = lda_object.run()

      for key, value in enumerate(lda_keywords):
          data_dict = {}
          for split_value in enumerate(value[1].split()):
              if split_value[1] != '+':
                  data_dict[split_value[1].split('*')[1].replace('"', '')] = float(split_value[1].split('*')[0])
          print(data_dict)
          key = str(key)
          chart = BarChart()
          chart.run("LDA - Temat " + key, "Prawdopodobienstwo", "Slowa",
                    data_dict, "lda" + key + "_chart")
          wordcloud = WordCloudChart()
          wordcloud.run(data_dict, "lda" + key + "_word_cloud")
          print("chart " + key + " generated")
          print("wordcloud " + key + " generated\n")

      #print (lda_keywords)
      print("\n\n")
    elif selection == '5':
      clean()
      print ("***************** LSA ******************")
      topics = int(input("Ile tematow wygenerowac: "))
      lsa_object = Lsa(stopwords, lematized_dir, topics)
      lsa_keywords = lsa_object.run()
      #print (lsa_keywords)
      for key, value in enumerate(lsa_keywords):
          data_dict = {}
          for split_value in enumerate(value[1].split()):
              if split_value[1] != '+':
                  data_dict[split_value[1].split('*')[1].replace('"', '')] = float(split_value[1].split('*')[0])
          print(data_dict)
          key = str(key)
          chart = BarChart()
          chart.run("LSA - Temat " + key, "Prawdopodobienstwo", "Slowa",
                    data_dict, "lsa" + key + "_chart")
          wordcloud = WordCloudChart()
          wordcloud.run(data_dict, "lsa" + key + "_word_cloud")
          print("chart " + key + " generated")
          print("wordcloud " + key + " generated\n")

      print("\n\n")
    elif selection == '6':
      clean()
      print ("**************** RAKE *****************")
      word_len = int(input("Ile minimum liter powinno miec slowo: "))
      phrase_words = int(input("Ile slow powinna miec fraza: "))
      appear_count = int(input("Ile razy slowo powinno wystapic w tekscie by byc uwzglednione: "))
      rake_object = Rake(stopwords, lematized_dir, word_len, phrase_words, appear_count)
      keywords = rake_object.run()
      data_dict = {}
      for key, value in enumerate(keywords):
          data_dict[value[0]] = value[1]
      chart = BarChart()
      chart.run("Rake", "Prawdopodobienstwo", "Slowa",  data_dict, 'rake_chart')
      wordcloud = WordCloudChart()
      wordcloud.run(data_dict, 'rake_word_cloud')
      print("chart generated")
      print("wordcloud generated")
      print(data_dict)
      print("\n\n")
    elif selection == '0': 
      break
    else: 
      clean()
      print ("Unknown Option Selected!")
